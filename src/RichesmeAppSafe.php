<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/7/8
 * Time: 17:08
 */

require_once __DIR__ . '/RichesmeAppRedis.php';
require_once __DIR__ . '/../../include/common.func.php';
/**
 * 安全控制 金额
 * Class RichesmeAppSafe
 */
class RichesmeAppSafe
{
    static private $instance = null;

    private function __construct()
    {
    }



    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new RichesmeAppSafe();

        }
        return self::$instance;
    }

    public function checkUserLogin()
    {

    }

    public function checkAuthoogin()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'OPTION') {
            header('HTTP/1.1 200');
        }//需要正确的回应前端预请求，否则前端不会发出真正的请求
        $username = 'add';          //帐号
        $passowrd = '123123';       //密码
        $realm = 'abc';            //密钥
        if (empty($_SERVER['HTTP_AUTHORIZATION']) && false) {//判断头部信息是否有添加
            header('HTTP/1.1 401 Unauthorization Required');    //401 此头弹出登录窗口
            header('WWW-Authenticate: Digest realm="' . $realm . '",qop="auth", nonce="' . uniqid() . '", opaque="' . md5($realm) . '"');
            die('您取消了本次登录，若重新登录，请刷新此页面。');
        } else {
            //使用函数http_digest_parse解析验证信息
            $data = $this->http_digest_parse($_SERVER['HTTP_AUTHORIZATION']);
            if (!($data = $this->http_digest_parse($_SERVER['HTTP_AUTHORIZATION'])) || $data['username'] != $username) {
                header("HTTP/1.1 401 Unauthorization Required");
                header('WWW-Authenticate: Digest realm="' . $realm . '",qop="auth", nonce="' . uniqid() . '", opaque="' . md5($realm) . '"');//IE 8 需要重新发送，不然不弹窗
                returnJsonStr(410211, '错误');
            }
            //拼接字符串
            $A1 = md5($username . ':' . $realm . ':' . $passowrd);
            $A2 = md5(strtoupper($_SERVER['REQUEST_METHOD']) . ':' . $data['uri']);
            $valid_response = md5($A1 . ':' . $data['nonce'] . ':' . $data['nc'] . ':' . $data['cnonce'] . ':' . $data['qop'] . ':' . $A2);
            if ($data['response'] != $valid_response) {
                header("HTTP/1.1 401 Unauthorization Required");
                header('WWW-Authenticate: Digest realm="' . $realm . '",qop="auth", nonce="' . uniqid() . '", opaque="' . md5($realm) . '"');
//                die('账号/密码错误！加密后的字符串和前端提交上来的不一样，肯定有某个地方两端没有统一');
                returnJsonStr(410213, '错误');
            }
            return true;
        }
// 解析字符串方法
        return false;
    }

    private function http_digest_parse($txt)
    {
        $needed_parts = array('nonce' => 1, 'nc' => 1, 'cnonce' => 1, 'qop' => 1, 'username' => 1, 'uri' => 1, 'response' => 1);
        $data = array();
        preg_match_all('@(\w+)=([\'"]?)([a-zA-Z0-9=./\_-]+)\2@', $txt, $matches, PREG_SET_ORDER);
        foreach ($matches as $m) {
            $data[$m[1]] = $m[3];
            $data['uri'] = $_SERVER['REQUEST_URI'];
            unset($needed_parts[$m[1]]);
        }
        return $data;
    }
}